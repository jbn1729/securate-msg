from flask import Flask, render_template, request
from numpy import array
import hashlib
import random
import os
def hasher(msg):
    return hashlib.sha256(msg.encode()).hexdigest()
LEN_IDENT_BYTES=256//8#256 bit
app = Flask(__name__)
def new(L):
	a=int.from_bytes(random.randbytes(LEN_IDENT_BYTES))
	while a in L:
		a=int.from_bytes(random.randbytes(LEN_IDENT_BYTES))
	return a
#crée le fichier si ce n'est pas déjà fait sinon ne fait rien
open('ips.txt', 'a').close()

@app.route("/", methods=['GET', 'POST'])
def recupe():
	ident = request.args.get("ident", default=0, type=int)
	if not ident:
		j = request.get_json(force=True)
		IP=hasher(request.remote_addr)#stocke l'adresse IP
		with open('ips.txt') as f:
			L = f.readlines()
		filtre=array(list(map(lambda line:line.split(), L)))
		if filtre.shape == (0,):
			I=new([])
			with open("ips.txt", "a") as f:
				f.write(" ".join((str(IP), str(I))))
			with open("keys/"+j[name]+".bin","w")as f:
				f.write(j["key"])
			return str(I)
		#print(filtre)
		if (I:=list(filtre[:, 0]).index(IP))!=-1:
			return str(filtre[I][1])
		else:
			I=new(filtre[:, 1])
			with open("ips.txt", "a") as f:
				f.write(" ".join((IP, I)))
			return str(I)
	else:
		all_dir = os.listdir('msgs/'+str(ident)+"/")
		for dir_ in all_dir:
			with open('msgs/'+str(ident)+'/'+dir_, 'r') as f:
				L=f.readlines()
			M[ident] = str(L)
		return str(M)

@app.route("/send", methods=['POST'])
def send():
	j = request.get_json(force=True)
	from_ = j["from"]
	to = j["to"]
	with open('msgs/'+from_+'/'+to+'.txt', 'a') as f:
		f.write(f["msg"])

@app.route('/keyto', method=["POST", "GET"])
def keyto():
	to = request.args.get("to", type=int)
	with open('keys/'+str(to)+".bin", "r") as f:
		bk = f.read()
	return str(int.from_bytes(bk))

if __name__ == '__main__':
	app.run(debug=True)
