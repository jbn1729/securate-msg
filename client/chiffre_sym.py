import hashlib

enter = "0123456789/*-+<>= éèàçabcdefghijklmnopqrstuvwxyz.!?;,:âêîôûäëïöüÉÈÀÇABCDEFGHIJKLMNOPQRSTUVWXYZÂÊÎÔÛÄËÏÖÜ'()\n"
ex = {'0':0, '1':1, '2':2, '3':3, '4':4, '5':5, '6':6, '7':7, '8':8, '9':9, '/':10, '*':11, '-':12, '+':13, '<':14, '>':15, '=':16, ' ':17, 'é':18, 'è':19, 'à':20, 'ç':21, 'a':22, 'b':23, 'c':24, 'd':25, 'e':26, 'f':27, 'g':28, 'h':29, 'i':30, 'j':31, 'k':32, 'l':33, 'm':34, 'n':35, 'o':36, 'p':37, 'q':38, 'r':39, 's':40, 't':41, 'u':42, 'v':43, 'w':44, 'x':45, 'y':46, 'z':47, '.':48, '!':49, '?':50, ';':51, ',':52, ':':53, 'â':54, 'ê':55, 'î':56, 'ô':57, 'û':58, 'ä':59, 'ë':60, 'ï':61, 'ö':62, 'ü':63, 'É':64, 'È':65, 'À':66, 'Ç':67, 'A':68, 'B':69, 'C':70, 'D':71, 'E':72, 'F':73, 'G':74, 'H':75, 'I':76, 'J':77, 'K':78, 'L':79, 'M':80, 'N':81, 'O':82, 'P':83, 'Q':84, 'R':85, 'S':86, 'T':87, 'U':88, 'V':89, 'W':90, 'X':91, 'Y':92, 'Z':93, 'Â':94, 'Ê':95, 'Î':96, 'Ô':97, 'Û':98, 'Ä':99, 'Ë':100, 'Ï':101, 'Ö':102, 'Ü':103, "'":104, '(':105, ')':106, "\n":107}
base = len(enter)
def hach(n):
	return int(hashlib.sha256(bytes(hex(n), "UTF-8")).hexdigest(), 16)

def chiffre_letter(c, dec):
	return (ex[c]+dec)%base

def dechiffre_letter(c, dec):
	return enter[(c-dec)%base]

def letter_to_n(l):
	ns=[]
	for c in l:
		ns.append(ex[c])
	return ns

def n_to_letter(n):
	res = ''
	while n:
		res = enter[n%base]+res
		n //= base
	return res

def chiffre(mot:str, clef:int):
	xmot = 0
	for i, m in enumerate(mot):
		xmot = xmot*107+chiffre_letter(m, hach(clef+i)%base)
	return n_to_letter(xmot)

def dechiffre(xmot, clef):
	xn = letter_to_n(xmot)
	mot=''
	for i, x in enumerate(xn):
		mot += dechiffre_letter(x, hach(clef+i)%base)
	return mot
