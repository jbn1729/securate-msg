import requests
from eaSIDH import lA, eA, lB, eB, keygen_Alice, keygenBob, params_Alice, splits_Alice, MAX_Alice, n_Bob, params_Bob, splits_Bob, MAX_Bob
from tkinter import *
from chiffre_sym import chiffre, dechiffre
########################utils########################
def n_to_letter_utf8(n):
	bs = b""
	while n:
		bs += (n&0xff).to_bytes()
		n >>= 8
	return bs
######################exchange#######################
clefs = {}
def req():
	req = requests.get("http://127.0.0.1:5000/?ident="+str(IDENT))
	return req.text

def public_key(name):
	req = requests.get("http://127.0.0.1:5000/keyof?name="+name)
	return req.text

def create_key(to):
	p_key = public_key(to)
	return shared_secret_Alice(private_key_Alice(), p_key, splits_Alice, MAX_Alice)

def send():
	from_ = IDENT
	to  = E_to.get()
	msg = E_msg.get()
	try:
		clef = clefs[to]
	except:
		clef = create_key(to)
		clefs[to] = clef
	xmsg = chiffre(msg, clef)
	req = requests.post("http://127.0.0.1:5000/send?ident={}"+str(IDENT), json={'from':from_, 'to':to, 'msg':xmsg})
########################ident########################
try:
	with open("ident.bin", "rb") as f:
		L = f.read()
	IDENT=int.from_bytes(L)
except:
	IDENT = int(requests.get("http://127.0.0.1:5000/"))
	with open("ident.bin", "wb") as f:
		f.write(n_to_letter_utf8(IDENT))
#########################key#########################
try:
	open("keyA.bin", "rb").close()
except:
	n_Alice = (randint(0,(lA**eA))|1)-1	#   send private key
	n_Bob= randint(0,lB**eB)			#receive private key
	keyA = keygen_Alice(n_Alice, params_Alice, splits_Alice, MAX_Alice)
	keyB = keygen_Bob(n_Bob, params_Bob, splits_Bob, MAX_Bob)
	with open("pivatekeyA.bin", "wb") as f:
		f.write(n_to_letter_utf8(n_Alice))
	with open("privatekeyB.bin", "wb") as f:
		f.write(n_to_letter_utf8(n_Bob))
	with open("publickeyA/0.bin", "wb") as f:
		f.write(n_to_letter_utf8keyA[0])
	with open("publickeyA/1.bin", "wb") as f:
		f.write(n_to_letter_utf8keyA[1]))
	with open("publickeyA/2.bin", "wb") as f:
		f.write(n_to_letter_utf8keyA[2]))
	with open("publickeyB/0.bin", "wb") as f:
		f.write(n_to_letter_utf8keyB[0]))
	with open("publickeyB/1.bin", "wb") as f:
		f.write(n_to_letter_utf8keyB[1]))
	with open("publickeyB/2.bin", "wb") as f:
		f.write(n_to_letter_utf8keyB[2]))

fen = Tk()
fen.geometry("500x500")
fen.title('securate message')
fen.resizable(False, False)

cv = Canvas(fen, height=500, width=500, bg='#FFFFFF')
cv.place(x=0, y=0)

B = Button(cv, text="réactualise", command=req)
B.place(x=0, y=0)
C = Button(cv, text="send", command=send)
C.place(x=220, y=0)


